import SurveyElement from '@perfect-data/survey/element';
import { validationSpec } from '@perfect-data/survey/decorators/element-validation';


const inputTypes = ['text', 'number', 'email', 'tel', 'time', 'date'];




/**
 * Input survey element. Element types need to extend this class and be registered.
 * 
 * Survey Element Model (inherites all from base SurveyElement)
 * 
 *    - inputType    : String   may be either 'text', 'number', 'email', 'tel', 'time', 'date' (default: 'text')
 *    - minLength    : Number
 *    - maxLength    : Number
 *    - pattern      : RegExp|String
 * 
 */
class SurveyElementInput extends SurveyElement {

   static type = 'input';

   static INPUT_TEXT = inputTypes[0];
   static INPUT_NUMBER = inputTypes[1];
   static INPUT_EMAIL = inputTypes[2];
   static INPUT_TEL = inputTypes[3];
   static INPUT_TIME = inputTypes[4];
   static INPUT_DATE = inputTypes[5];


   @validationSpec('type')
   _inputType = SurveyElementInput.INPUT_TEXT;

   @validationSpec('minLength')
   _minLength = undefined

   @validationSpec('maxLength')
   _maxLength = undefined;

   @validationSpec('match')
   _pattern = undefined;


   /**
    * Create a new input element
    * @param {Object} model 
    */
   constructor(model) {
      super(model);

      // note : model must specify the file name, so we assume that it is set at this point
      if ('inputType' in model) {
         this.setInputType(model.inputType);
      }
      if ('minLength' in model) {
         this.setMinLength(model.minLength);
      }
      if ('maxLength' in model) {
         this.setMaxLength(model.maxLength);
      }
      if ('pattern' in model) {
         this.setMatchPattern(model.pattern);
      }
   }

   /**
    * Return the type of input to use for this element
    * @return {String}
    */
   getInputType() {
      return this._inputType;
   }

   /**
    * Set the type of input to use for this element.
    * Possible values are : 'text', 'number', 'email', 'tel', 'time', 'date'
    */
   setInputType(inputType) {
      if (typeof inputType === 'string') {
         inputType = inputType.toLocaleLowerCase();
      }
      if (!inputTypes.includes(inputType)) {
         throw new TypeError('Invalid input type : ' + inputType);
      }

      this._inputType = inputType;
   }

   /**
    * Return the minimum input length constraint
    * @return {Number|undefined}
    */
   getMinLength() {
      return this._minLength || 0;
   }

   /**
    * Set the minimum input length constraint
    * @param {Number|undefined} minLength
    */
   setMinLength(minLength) {
      if (minLength < 0) {
         throw new TypeError('Value must not be negative');
      }

      this._minLength = Number(minLength) || 0;

      if (this._minLength > this._maxLength) {
         this._maxLength = this._minLength;
      }
   }

   /**
    * Return the manimum input length constraint
    * @return {Number|undefined}
    */
   getMaxLength() {
      return this._maxLength !== undefined ? this._maxLength : Infinity;
   }

   /**
    * Set the manimum input length constraint
    * @param {Number|undefined} manLength
    */
   setMaxLength(maxLength) {
      if (maxLength < 0) {
         throw new TypeError('Value must not be negative');
      }

      this._maxLength = Number(maxLength) || 0;

      if (this._maxLength < this._minLength) {
         this._minLength = this._maxLength;
      }
   }

   /**
    * Return the validation pattern to match
    * @return {String|RegExp}
    */
   getMatchPattern() {
      return this._pattern;
   }

   /**
    * Set the validation pattern to match
    * @param {String|RegExp} pattern 
    */
   setMatchPattern(pattern) {
      if (typeof pattern === 'string') {
         pattern = new RegExp(pattern);
      }

      this._pattern = pattern;
   }

   /**
    * Return a JSON object of this element
    * @returns {Object}
    */
   toJSON() {
      const data = {
         ...super.toJSON(),
         inputType: this._inputType
      };

      if (this._minLength !== undefined) {
         data.minLength = this._minLength;
      }
      if (this._maxLength !== undefined ) {
         data.maxLength = this._maxLength;
      }
      if (this._pattern) {
         data.match = this._pattern;
      }

      return data;
   }

}

export default SurveyElementInput;