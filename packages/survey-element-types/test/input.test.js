import SurveyModel from '@perfect-data/survey/model';
import { validationSpec, getValidationSpecs } from '@perfect-data/survey/decorators/element-validation';
import SurveyElementInput from '../src/input';


describe('Testing Input Survey Element', () => {

   it('should create instance', () => {
      expect(() => new SurveyElementInput({ name: 'testField' })).not.toThrow();
   });

   it('should fail to create instance if no name given', () => {
      expect(() => new SurveyElementInput({ })).toThrow();
   });

   it('should validate SurveyElement prototype', () => {
      const field = new SurveyElementInput({ name: 'testField' });

      expect(SurveyElementInput.type).toBe('input');
      expect(field.getType()).toBe(SurveyElementInput.type);

      expect(field.getInputType()).toBe(SurveyElementInput.INPUT_TEXT);
      expect(field.getMinLength()).toBe(0);
      expect(field.getMaxLength()).toBe(Infinity);
      expect(field.getMatchPattern()).toBe(undefined);

      field.setValidationMessage('required', 'Test is required');
      expect(field.getValidationMessage('required')).toBe('Test is required');
   });

   it('should create instance with set options', () => {
      const field = new SurveyElementInput({ 
         name: 'testField',

         inputType: SurveyElementInput.INPUT_NUMBER,
         minLength: 4,
         maxLength: 10,
         pattern: 'test'      
      });

      expect(field.getInputType()).toBe(SurveyElementInput.INPUT_NUMBER);
      expect(field.getMinLength()).toBe( 4 );
      expect(field.getMaxLength()).toBe( 10 );
      expect(field.getMatchPattern()).toEqual( /test/ );
   });

   it('should set inputType', () => {
      const field = new SurveyElementInput({ name: 'testField' });

      [
         SurveyElementInput.INPUT_DATE,
         SurveyElementInput.INPUT_EMAIL,
         SurveyElementInput.INPUT_NUMBER,
         SurveyElementInput.INPUT_TEL,
         SurveyElementInput.INPUT_TEXT,
         SurveyElementInput.INPUT_TIME
      ].forEach(inputType => {
         field.setInputType(inputType);
         expect( field.getInputType() ).toBe(inputType);
      });
   });

   it('should fail to set invalid inputType', () => {
      const field = new SurveyElementInput({ name: 'testField' });

      [
         undefined, NaN, null, true, false, -1, 0, 1, 
         () => {}, [], {}, /./, new Date(),
         '', 'test', '$'
      ].forEach(inputType => {
         expect(() => field.setInputType(inputType)).toThrow();
      });
   });

   it('should set min/max length', () => {
      const field = new SurveyElementInput({ name: 'testField' });

      field.setMinLength(10);
      field.setMaxLength(20);

      expect(field.getMinLength()).toBe(10);
      expect(field.getMaxLength()).toBe(20);

      field.setMinLength(30);  // adjust max length

      expect(field.getMinLength()).toBe(30);
      expect(field.getMaxLength()).toBe(30);

      field.setMaxLength(15);  // adjust min length

      expect(field.getMinLength()).toBe(15);
      expect(field.getMaxLength()).toBe(15);

      field.setMinLength(5);

      expect(field.getMinLength()).toBe(5);
      expect(field.getMaxLength()).toBe(15);

      // special : set to default = 0
      field.setMinLength();
      expect(field.getMinLength()).toBe(0);

      field.setMaxLength();
      expect(field.getMaxLength()).toBe(0);
   });

   it('should fail setting min/max length', () => {
      const field = new SurveyElementInput({ name: 'testField' });

      [
         -1
      ].forEach(value => {
         expect(() => field.setMinLength(value)).toThrow();
         expect(() => field.setMaxLength(value)).toThrow();
      });
   });

   it('should set pattern', () => {
      const field = new SurveyElementInput({ name: 'testField' });

      field.setMatchPattern(/test/);
      expect(field.getMatchPattern()).toEqual(/test/);

      field.setMatchPattern('test2');
      expect(field.getMatchPattern()).toEqual(/test2/);
   });

   it('should return JSON', () => {
      const field = new SurveyElementInput({ name: 'testField' });

      expect(field.toJSON()).toEqual({
         name: 'testField',
         type: 'input',
         inputType: 'text'
      });

      field.setMatchPattern('test');
      field.setInputType(SurveyElementInput.INPUT_NUMBER);

      expect(field.toJSON()).toEqual({
         name: 'testField',
         type: 'input',
         inputType: 'number',
         match: /test/
      });

      field.setMinLength(6);

      expect(field.toJSON()).toEqual({
         name: 'testField',
         type: 'input',
         inputType: 'number',
         minLength: 6,
         match: /test/
      });

      expect(field.toJSON()).not.toHaveProperty('maxLength');

      field.setMaxLength(5);

      expect(field.toJSON()).toEqual({
         name: 'testField',
         type: 'input',
         inputType: 'number',
         minLength: 5,
         maxLength: 5,
         match: /test/
      });
   });

   describe('Validation', () => {

      it('should return validation specs', () => {
         const field = new SurveyElementInput({ name: 'testField' });
         const specs = getValidationSpecs(field);

         expect( specs ).toEqual(expect.objectContaining({
            required: false
         }));

         expect( specs ).not.toHaveProperty('minLength');
         expect( specs ).not.toHaveProperty('maxLength');
         expect( specs ).not.toHaveProperty('match');
      });

      it('should return custum validation specs', () => {
         const field = new SurveyElementInput({ name: 'testField', minLength: 3 });
         const specs = getValidationSpecs(field);

         expect( specs ).toEqual(expect.objectContaining({
            required: false,
            minLength: 3
         }));

         expect( specs ).not.toHaveProperty('maxLength');
         expect( specs ).not.toHaveProperty('match');
      });

   });

});