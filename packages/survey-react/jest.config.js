
module.exports = {
   verbose: true,
   globals: {
      "NODE_ENV": "test"
   },
   clearMocks: true,
   coverageDirectory: "coverage",
   testEnvironment: "jsdom",
   transform: {
      "^.+\\.jsx?$": "babel-jest"
   },
   moduleFileExtensions: [
      "js", "jsx", "json"
   ],
   moduleDirectories: [
      "node_modules",
      "src/",
   ]
};