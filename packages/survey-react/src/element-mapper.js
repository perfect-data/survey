import InputElement from '@perfect-data/survey-element-types/input';
import InputComponent from './components/Input';

const mapping = {
   [InputElement.type]: InputComponent
};

export default mapping;