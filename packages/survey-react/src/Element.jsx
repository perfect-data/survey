import React, { useState, useContext } from 'react';
import PropTypes from 'prop-types';

import SurveyElement from '@perfect-data/survey/element';

import { ResponseContext } from './context/response';

import elementMapping from './element-mapper';


const ElementComponent = ({ 
   element, 
   value,
   onChange,
   ...props
}) => {
   const responseContext = useContext(ResponseContext);
   const Element = elementMapping[element.getType()];
   
   // override
   value = responseContext.getResponseValue(element.getName()) || value;

   const handleValueChanged = event => {
      responseContext.setResponseValue(element.getName(), event.target.value);

      if (onChange) {
         onChange(event);
      }
   };

   return Element ? (
      <Element name={ element.getName() } value={ value } onChange={ handleValueChanged } { ...props } />
   ) : (
      "Error! Missing Element type : " + element.getType()
   );
};

ElementComponent.propTypes = {
   element: PropTypes.instanceOf(SurveyElement).isRequired,
   onChange: PropTypes.func
};


export default ElementComponent;