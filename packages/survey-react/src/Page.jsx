import React from 'react';
import PropTypes from 'prop-types';

import SurveyPage from '@perfect-data/survey/page';
import Element from './Element';


const PageComponent = ({ 
   page,
   elementProps,
   ... props
}) => (
   <div { ...props }>
      { page && page.getElementCount() ? page.getElements().map((element, index) => <Element key={ index } element={ element } { ...elementProps } />) : '' }
   </div>
);

PageComponent.propTypes = {
   page: PropTypes.instanceOf(SurveyPage).isRequired,
   elementProps: PropTypes.object
};


PageComponent.defaultProps = {
   elementProps: {}
};


export default PageComponent;