import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';


const InputElement = ({ 
   onChange, 
   value: initialValue, 
   ... props
}) => {
   const [ value, setValue ] = useState('');

   useEffect(() => {
      setValue(initialValue);
   }, [initialValue]);

   function handleChange(event) {
      setValue(event.target.value);

      if (onChange) {
         onChange(event);
      }
   }

   return (
      <input onChange={ handleChange } value={ value } { ...props } />
   );
};


InputElement.propTypes = {
   type: PropTypes.string,
   onChange: PropTypes.func,
   value: PropTypes.any
};

InputElement.defaultProps = {
   type: 'text'
};


export default InputElement;