import { createContext } from 'react';


export const ResponseContext = createContext({
   getResponseValue: (name) => {},
   setResponseValue: (name, value) => {}
});
