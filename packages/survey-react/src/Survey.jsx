import React, { useState } from 'react';
import PropTypes from 'prop-types';

import SurveyModel from '@perfect-data/survey';

import { ResponseContext } from './context/response';

import Page from './Page';


const SurveyComponent = ({
   survey,
   pageProps,
   onResponseChange,
   ... props
}) => {
   const [ responseData, setResponseData ] = useState({});
   const contextValue = {
      getResponseValue: (name) => responseData[name],
      setResponseValue: (name, value) => {
         const newResponseData = { ...responseData, [name]: value };

         if (onResponseChange) {
            onResponseChange(newResponseData);
         }

         setResponseData(newResponseData);
      }
   };
   
   return (  
      <div { ...props }>
         <ResponseContext.Provider value={ contextValue }>
            { survey && survey.getPageCount() ? survey.getPages().map((page, index) => <Page key={ index } page={ page } { ...pageProps } />) : '' }
         </ResponseContext.Provider>
      </div>
   );
};

SurveyComponent.propTypes = {
   survey: PropTypes.instanceOf(SurveyModel).isRequired,
   pageProps: PropTypes.object,
   onResponseChange: PropTypes.func
};

SurveyComponent.defaultprops = {
   pageProps: {}
};


export default SurveyComponent;