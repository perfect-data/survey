import React from 'react';
import Enzime, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SurveyModel from '@perfect-data/survey';
import SurveyElement from '@perfect-data/survey/element';
import Survey from '../src/Survey';

Enzime.configure({ adapter: new Adapter() });

describe('Testing default input field component', () => {

   class TestSurveyElement extends SurveyElement {
      static type = 'testType';

   };

   SurveyElement.registerType(TestSurveyElement);


   it('should display model', () => {
      const survey = new SurveyModel({
         title: 'Test Survey',
         pages: [
            { elements: [
               { type: 'testType', name: 'test1' }
            ] },
            { elements: [
               { type: 'testType', name: 'test2' }
            ] }
         ]
      });

      const wrapper = shallow(<Survey survey={ survey } />);

      expect(wrapper.exists()).toBe(true);

   });

});