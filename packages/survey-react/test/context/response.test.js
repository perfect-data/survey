import React, { useContext } from 'react';
import Enzime, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { ResponseContext } from '../../src/context/response';

Enzime.configure({ adapter: new Adapter() });


describe('Testing response context', () => {

   it('should be a valid Provider/Consumer', () => {

      expect(ResponseContext).toHaveProperty('Provider');
      expect(ResponseContext).toHaveProperty('Consumer');

   });

   it('should provide default behavior', () => {
      const TestConsumer = () => {
         const response = useContext(ResponseContext);

         expect(response).toHaveProperty('getResponseValue');
         expect(response).toHaveProperty('setResponseValue');

         expect(response.getResponseValue()).toBe(undefined);
         expect(response.setResponseValue()).toBe(undefined);

         return <div className="test"></div>;
      }

      const wrapper = shallow(
         <TestConsumer />
      );

      expect(wrapper.exists()).toBe(true);
      expect(wrapper.find('.test').length).toBe(1);
   });

});