import React from 'react';
import Enzime, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import InputElement from '../../src/components/Input';

Enzime.configure({ adapter: new Adapter() });

describe('Testing default input field component', () => {

   it('should return DOMInputElement', () => {
      const wrapper = shallow(<InputElement />);

      expect(wrapper.exists()).toBe(true);

      wrapper.find('input').simulate('change', { target: { value: 'test' } });

      expect(wrapper.find('input').props().value).toEqual('test');
      expect(wrapper.find('input').props().type).toEqual('text');
   });

   it('should handle change events', () => {
      const wrapper = shallow(<InputElement onChange={ testChange } />);
      let handled = false;

      function testChange(event) {
         expect(event).toHaveProperty('target');
         expect(event.target).toHaveProperty('value', 'test');

         handled = true;
      };

      expect(wrapper.exists()).toBe(true);

      wrapper.find('input').simulate('change', { target: { value: 'test' } });

      expect(handled).toBe(true);      
   });

   // No support for useEffect in Enzime
   // See : https://github.com/facebook/react/issues/15275
   //it('should set value', () => {
   //   const wrapper = shallow(<InputElement value={ 'test' } />);
   //
   //   expect(wrapper.exists()).toBe(true);
   //
   //   expect( wrapper.find('input').prop('value') ).toBe('test');
   //});

});