import React from 'react';
import Enzime, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PageComponent from '../src/Page';

import SurveyPage from '@perfect-data/survey/page';
import SurveyElement from '@perfect-data/survey/element';

import elementMapper from '../src/element-mapper';


Enzime.configure({ adapter: new Adapter() });

describe('Testing default page component', () => {

   class MockSurveyElement extends SurveyElement { 
      static type = 'testType';
   }

   beforeAll(() => {
      elementMapper[MockSurveyElement.type] = ({ 
         onChange, 
         value, 
         ...props
      }) => (
         <div { ...props } onChange={ onChange }>{ value }</div>
      );
   });

   afterAll(() => {
      elementMapper[MockSurveyElement.type] = null;
   });


   it('should return DOMElement', () => {
      const page = new SurveyPage();
      const wrapper = shallow(<PageComponent className="test" page={ page } />);

      expect(wrapper.exists()).toBe(true);
      expect(wrapper.find('.test').length).toBe(1);
   });

   //it('should have children', () => {
   //   const wrapper = shallow(<PageComponent><section>Child 1</section><section>Child 2</section></PageComponent>);
   //
   //   expect(wrapper.find('section').length).toBe(2);
   //});

   it('should render elements', () => {

      const page = new SurveyPage({
         elements: [
            new MockSurveyElement({ name: 'element1' }),
            new MockSurveyElement({ name: 'element2' }),
            new MockSurveyElement({ name: 'element3' })
         ]
      });
      const elementProps = {
         className: 'test-element'
      };
      const wrapper = shallow(<PageComponent className="test" page={ page } elementProps={ elementProps } />);

      expect(wrapper.exists()).toBe(true);
      expect(wrapper.find('.test-element').length).toBe(3);

   });

});