import React from 'react';
import Enzime, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SurveyComponent from '../src/Survey';

import SurveyModel from '@perfect-data/survey/model';
import SurveyElement from '@perfect-data/survey/element';

import elementMapper from '../src/element-mapper';


Enzime.configure({ adapter: new Adapter() });

describe('Testing Survey component', () => {

   class MockSurveyElement extends SurveyElement { 
      static type = 'testType';
   }

   beforeAll(() => {
      elementMapper[MockSurveyElement.type] = ({ 
         onChange, 
         value, 
         ...props
      }) => (
         <input { ...props } onChange={ onChange } value={ value || '' } />
      );
   });

   afterAll(() => {
      elementMapper[MockSurveyElement.type] = null;
   });


   it('should return DOMElement', () => {
      const survey = new SurveyModel();
      const wrapper = shallow(<SurveyComponent className="test" survey={ survey } />);

      expect(wrapper.exists()).toBe(true);
      expect(wrapper.find('.test').length).toBe(1);
   });


   it('should get/set response values', () => {
      const survey = new SurveyModel({
         pages: [
            {
               elements: [
                  new MockSurveyElement({ name: 'element1' }),
                  new MockSurveyElement({ name: 'element2' })
               ]
            }
         ]
      });
      const extraProps = {
         pageProps: {
            elementProps: {
               className: 'testElement'
            }
         }
      }
      const wrapper = mount(<SurveyComponent survey={ survey } { ...extraProps } />);
      
      expect(wrapper.exists()).toBe(true);
      expect(wrapper.find('input').length).toBe(2);
   });


   it('should receive responseChange events', () => {
      const survey = new SurveyModel({
         pages: [
            {
               elements: [
                  new MockSurveyElement({ name: 'element1' }),
               ]
            }
         ]
      });
      const extraProps = {
         pageProps: {
            elementProps: {
               className: 'testElement'
            }
         },
         onResponseChange: response => {
            expect(response).toEqual({ 'element1': 'simulated' });
            responseChanged = true;
         }
      };
      const wrapper = mount(<SurveyComponent survey={ survey } { ...extraProps } />);
      let responseChanged = false;
      
      expect(wrapper.exists()).toBe(true);
      expect(wrapper.find('input[name="element1"]').length).toBe(1);

      wrapper.find('input[name="element1"]').simulate('change', { target: { value: 'simulated' } });

      expect(responseChanged).toBe(true);
   });


});