import React from 'react';
import Enzime, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ElementComponent from '../src/Element';

import SurveyElement from '@perfect-data/survey/element';

import elementMapper from '../src/element-mapper';


Enzime.configure({ adapter: new Adapter() });

describe('Testing default page component', () => {

   class MockSurveyElement extends SurveyElement { 
      static type = 'testType';
   }

   beforeAll(() => {
      elementMapper[MockSurveyElement.type] = ({ 
         onChange, 
         value, 
         ...props
      }) => (
         <div { ...props } onChange={ onChange }>{ value }</div>
      );
   });

   afterAll(() => {
      elementMapper[MockSurveyElement.type] = null;
   });


   it('should return DOMElement', () => {
      const element = new MockSurveyElement({ name: 'test' });
      const wrapper = shallow(<ElementComponent className="test" element={ element } />);

      expect(wrapper.exists()).toBe(true);
      expect(wrapper.find('.test').length).toBe(1);
   });

   it('should emit onChange events', () => {
      const element = new MockSurveyElement({ name: 'test' });
      const wrapper = shallow(<ElementComponent className="test" element={ element } onChange={ handleChange } />);
      let handled = false;

      function handleChange(event) {
         expect(event).toHaveProperty('target');
         expect(event.target).toHaveProperty('value', 'test');

         handled = true;
      }

      wrapper.find('.test').simulate('change', { target: { value: 'test' } });

      expect(handled).toBe(true);
   });

   it('should not complain if no onChange handler is provided', () => {
      const element = new MockSurveyElement({ name: 'test' });
      const wrapper = shallow(<ElementComponent className="test" element={ element } />);

      wrapper.find('.test').simulate('change', { target: { value: 'test' } });
   });


   it('should set value', () => {
      const element = new MockSurveyElement({ name: 'test' });
      const wrapper = shallow(<ElementComponent className="test" element={ element } value="test" />);

      expect(wrapper.find('.test').html()).toBe("<div name=\"test\" class=\"test\">test</div>");
      //console.log( wrapper.html() );
   });


   it('should fallback gracefully on missing element type', () => {
      class DummySurveyElement extends SurveyElement { 
         static type = 'dummyType';
      }
      const element = new DummySurveyElement({ name: 'dummy' });
      const wrapper = shallow(<div><ElementComponent className="dummy" element={ element } /></div>);

      expect(wrapper.exists()).toBe(true);
      expect(wrapper.html()).toBe("<div>Error! Missing Element type : dummyType</div>");
   })

});