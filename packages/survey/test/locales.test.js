import SurveyLocales, { LocalizedAbstract, Locales } from '../src/locales';


describe('Testing Survey Locales', () => {

   it('should provide Locales interface', () => {
      expect( Locales ).not.toBe(undefined);
      expect( Locales.validate('en') ).toBe(true);
   });

   describe('Testing SurveyLocales', () => {

      it('should get and set default locale', () => {
         const lang = SurveyLocales.getDefaultLanguage();

         expect(() => Locales.validate(lang)).not.toThrow();

         SurveyLocales.setDefaultLanguage('fr');
         
         expect( SurveyLocales.getDefaultLanguage() ).toBe('fr');

         SurveyLocales.setDefaultLanguage(lang);
      });

      it('should not allow setting invalid locales', () => {
         [
            undefined, null, NaN, 0, 1, 'zyz'
         ].forEach(lang => expect(() => SurveyLocales.setDefaultLanguage(lang)).toThrow());
      });

   });


   describe('TEsting LocalizedAbstract', () => {

      class MockLocalized extends LocalizedAbstract {}

      it('should not allow direct instance', () => {
         expect(() => new LocalizedAbstract()).toThrow();
      });

      it('should return empty JSON if no strings', () => {
         const localized = new MockLocalized();

         expect(localized.toJSON()).toEqual({});
      });

      it('should set default localized string', () => {
         const localized = new MockLocalized();

         localized.setString('test', 'This is a test');

         expect(localized.toJSON()).toEqual({
            strings: {
               [SurveyLocales.getDefaultLanguage()]: {
                  'test': 'This is a test'
               }
            }
         });
      });

      it('should set both default and custom localized string', () => {
         const localized = new MockLocalized();

         localized.setString('test', 'This is a test');
         localized.setString('test', 'Ceci est un test', 'fr');

         expect(localized.toJSON()).toEqual({
            strings: {
               [SurveyLocales.getDefaultLanguage()]: {
                  'test': 'This is a test'
               },
               'fr': {
                  'test': 'Ceci est un test'
               }

            }
         });
      });

      it('should create instance with default strings', () => {
         const strings = { 'en': 'Test EN', 'fr': 'Test FR' };
         const localized = new MockLocalized({ strings });

         expect(localized.toJSON()).toEqual({ strings });
      });

      it('should ignore invalid strings', () => {
         [
            undefined, null, NaN, 0, -1, 1, [], ''
         ].forEach(strings => {
            const localized = new MockLocalized({ strings });

            expect(localized.toJSON()).toEqual({});
         });
      });

      it('should fail setting localized string with invalid language', () => {
         const localized = new MockLocalized();

         [
            null, false, true, NaN, 0, -1, {}, '', 'zyz', new Date()
         ].forEach(lang => expect(() => localized.setString('test', 'Should not work', lang)).toThrow());

         expect(localized.toJSON()).toEqual({});
      });

      it('should fail at instanciating with invalid language strings', () => {
         [
            undefined, null, NaN, 0, -1, 1, [], ''
         ].forEach(lang => {
            const strings = { [lang]: { 'test': 'Invalid' } };

            expect(() => new MockLocalized({ strings })).toThrow();
         });
      });

      it('should fallback to default on missing string', () => {
         const localized = new MockLocalized();

         localized.setString('test', 'Test EN');

         expect(localized.getString('test', 'fr')).toBe('Test EN');
      });

      it('should fallback language on missing string', () => {
         const localized = new MockLocalized();

         localized.setString('test', 'Test FR', 'fr');

         expect(localized.getString('test')).toBe('Test FR');
      });

      it('should fail if key is not a string', () => {
         const localized = new MockLocalized();

         [
            undefined, null, {}, 0, -1, NaN, false, true, () => {}, [], new Date()
         ].forEach(key => expect(() => localized.setString(key, 'Test')).toThrow());
      });

      it('should fail if string is not a string', () => {
         const localized = new MockLocalized();

         [
            undefined, null, {}, 0, -1, NaN, false, true, () => {}, [], new Date()
         ].forEach(string => expect(() => localized.setString('test', string)).toThrow());
      });

      it('should retrieve all known languages', () => {
         const localized = new MockLocalized();

         localized.setString('test', 'Test EN', 'en');
         localized.setString('dummy', 'Dummy FR', 'fr');

         expect(localized.getLanguages()).toEqual(['en', 'fr']);

      });

   });

});