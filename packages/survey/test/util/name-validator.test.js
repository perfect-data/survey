import nameValidator from '../../src/util/name-validator';


describe('Testing name validator', () => {

   it('should validate', () => {
      [
         'a', 'a123',
         'foo', 'foo0',
         'hello_world', 'hello_world1',
         'someVeryLongCamelCaseText'
      ].forEach(name => expect(nameValidator(name)).toBeTruthy());
   });

   it('should fail', () => {
      [
         '', '_',
         '1', '1_',
         '1hello_'
      ].forEach(name => expect(nameValidator(name)).toBeFalsy());
   });

});