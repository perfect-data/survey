import { validationSpec, getValidationSpecs } from '../../src/decorators/element-validation';

describe('Testing decorator : element-validation', () => {

   it('should define validators with initializer and options', () => {
      const surveyDummy = { test: 'test' };
      const initializer = (value, target, survey, options) => ({ value, target, survey, options });
      const options = 'options';

      const validator = validationSpec('testSpec', initializer, options);
      const target = {
         get test() { return 'test'; }
      };

      validator(target, 'test', Object.getOwnPropertyDescriptor(target, 'test'));

      const spec = getValidationSpecs(target, surveyDummy);

      expect( spec ).toHaveProperty('testSpec');
      expect( spec.testSpec ).toEqual({
         value: 'test',
         target,
         survey: surveyDummy,
         options
      });
   });

   it('should define validators without initializer', () => {
      const validator = validationSpec('testSpec');
      const target = {
         get test() { return 'test'; }
      };

      validator(target, 'test', Object.getOwnPropertyDescriptor(target, 'test'));

      const spec = getValidationSpecs(target);

      expect( spec ).toHaveProperty('testSpec');
      expect( spec.testSpec ).toEqual('test');
   });

   it('should define multiple validations', () => {
      const validator1 = validationSpec('testSpec1');
      const validator2 = validationSpec('testSpec2');
      const target = {
         test1() { return 'test1'; },
         test2: 'test2'
      };

      validator1(target, 'test1', Object.getOwnPropertyDescriptor(target, 'test1'));
      validator2(target, 'test2', Object.getOwnPropertyDescriptor(target, 'test2'));

      const spec = getValidationSpecs(target);

      expect( spec ).toHaveProperty('testSpec1');
      expect( spec ).toHaveProperty('testSpec2');

      expect( spec.testSpec1 ).toEqual('test1');
      expect( spec.testSpec2 ).toEqual('test2');
   });

   it('should fail to get spec if not validator was defined', () => {
      const target = {
         test1() { return 'test1'; },
         test2: 'test2'
      };

      expect(() => getValidationSpecs(target)).toThrow();
   });

   it('should get updated property value', () => {
      const validator = validationSpec('testSpec');
      const target = {
         test: 'hello'
      };

      validator(target, 'test', Object.getOwnPropertyDescriptor(target, 'test'));

      target.test = 'world';

      const spec1 = getValidationSpecs(target);

      expect( spec1 ).toHaveProperty('testSpec');
      expect( spec1.testSpec ).toEqual('world');

      // setting undefined...

      target.test = undefined;

      const spec2 = getValidationSpecs(target);

      expect( spec2 ).not.toHaveProperty('testSpec');

      // resetting value to a function

      target.test = () => "I has returned!";

      const spec3 = getValidationSpecs(target);

      expect( spec3 ).toHaveProperty('testSpec');
      expect( spec3.testSpec ).toEqual('I has returned!');

   });

});
