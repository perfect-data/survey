import SurveyPage from '../src/page';
import SurveyElement from '../src/element';


describe('Testing Survey Page', () => {

   beforeAll(() => {
      class TestSurveyElement extends SurveyElement {
         static type = 'testType';
      }

      SurveyElement.registerType(TestSurveyElement);
   });

   afterAll(() => {
      SurveyElement.unregisterType('testType');
   });


   it('should create new survey page', () => {
      const page = new SurveyPage();

      expect(page.getElementCount()).toBe(0);
      expect(page.hasRandomElements()).toBe(false);

      page.setRandomElements();

      expect(page.hasRandomElements()).toBe(true);

      expect(page.toJSON()).toEqual({
         randomElements: true,
         elements: []
      });
   });

   it('should create randomized page', () => {
      const page = new SurveyPage({ randomElements: true });

      expect(page.getElementCount()).toBe(0);
      expect(page.hasRandomElements()).toBe(true);

      expect(page.toJSON()).toEqual({
         randomElements: true,
         elements: []
      });
   });

   it('should add and remove elements', () => {
      class DummySurveyElement extends SurveyElement {
         static type = 'dummyType';
      }

      const page = new SurveyPage({
         elements: [
            {
               type: 'testType',
               name: 'test1'
            },
            new DummySurveyElement({ name: 'dummy1' })
         ]
      });


      expect(page.toJSON()).toEqual({
         randomElements: false,
         elements: [
            {
               type: 'testType',
               name: 'test1'
            },
            {
               type: 'dummyType',
               name: 'dummy1'
            }
         ]
      });

      expect(page.getElement(0)).toBeInstanceOf(SurveyElement);
      expect(page.getElement(1)).toBeInstanceOf(DummySurveyElement);

      expect(page.getElements().length).toBe(2);

      page.addElement(new DummySurveyElement({ name: 'dummy2' }));
      page.addElement({
         type: 'testType',
         name: 'test2'
      }, 1);

      expect(page.getElements().length).toBe(4);
      // Note
      // 0. TestSurveyElement
      // 1. TestSurveyElement
      // 2. DummySurveyElement
      // 3. DummySurveyElement

      page.removeElement(0);
      page.removeElement(0);

      expect(page.getElements().length).toBe(2);

      expect(page.getElement(0)).toBeInstanceOf(DummySurveyElement);
      expect(page.getElement(1)).toBeInstanceOf(DummySurveyElement);
   });


});