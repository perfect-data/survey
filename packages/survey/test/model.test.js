import SurveyModel from '../src/model';
import SurveyPage from '../src/page';
import SurveyElement from '../src/element';


describe('Testing Survey Model', () => {

   it('should create new instance', () => {
      const model = new SurveyModel();

      expect(model.getPages()).toEqual([]);
      expect(model.getTitle()).toBeUndefined();

      model.setTitle('Test survey');

      expect(model.getTitle()).toEqual('Test survey');
   });

   it('should create new instance with title', () => {
      const model = new SurveyModel({
         title: 'Sample Survey'
      });

      expect(model.getTitle()).toEqual('Sample Survey');
   });

   it('should create new instance with pages', () => {
      const pages = [
         {},
         new SurveyPage()
      ];

      const model = new SurveyModel({ pages });

      expect(model.getPageCount()).toBe(2);
      expect(model.getPage(0)).toBeInstanceOf(SurveyPage);
      expect(model.getPage(1)).toBeInstanceOf(SurveyPage);

      const page3 = new SurveyPage();

      model.addPage(page3);

      expect(model.getPageCount()).toBe(3);
      expect(model.getPage(2)).toBe(page3);

      model.addPage({ randomElements: true }, 1);

      expect(model.getPageCount()).toBe(4);
      expect(model.getPage(1).hasRandomElements()).toBe(true);

      model.removePage(3);

      expect(model.getPageCount()).toBe(3);
      expect(model.getPages()).not.toBe(expect.arrayContaining([page3]));

      expect(model.toJSON()).toEqual({
         pages: [
            { elements: [], randomElements: false },
            { elements: [], randomElements: true },
            { elements: [], randomElements: false }
         ]
      });
   });

   it('should create schema', () => {
      class TestSurveyElement extends SurveyElement {
         static type = 'testType';
      };

      SurveyElement.registerType(TestSurveyElement);

      const page1 = {
         elements: [
            { type: 'testType', name: 'field1' },
            { type: 'testType', name: 'field2' }
         ]
      };
      const page2 = {
         elements: [
            { type: 'testType', name: 'field3' },
            { type: 'testType', name: 'field4' }
         ]
      };

      const model = new SurveyModel({ pages: [ page1, page2 ] });

      expect(model.getPageCount()).toBe(2);
      expect(model.hasElement('field3')).toBe(true);
      expect(model.hasElement('missing')).toBe(false);

      const schema = model.createSchema();

      expect(schema.fieldNames).toEqual(expect.arrayContaining(['field1', 'field2', 'field3', 'field4']));

      SurveyElement.unregisterType('testType');
   });

});