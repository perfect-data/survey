import { SchemaType } from '@perfect-data/schema';

import SurveyElement from '../src/element';
import SurveyLocales from '../src/locales';
import { getValidationSpecs } from '../src/decorators/element-validation';


describe('Testing Survey Element', () => {

   it('should not allow direct instanciation', () => {
     expect(() => new SurveyElement()).toThrowError('Cannot instanciate this class directly');
   });

   it('should fail with no defined type', () => {
      class MockSurveyElement extends SurveyElement { }

      expect(() => new MockSurveyElement({ name: 'test' })).toThrowError('Type not defined');
   });

   it('should fail with invalid model', () => {
      class MockSurveyElement extends SurveyElement { 
         static type = 'testType';
      }

      expect(() => new MockSurveyElement()).toThrowError('Element model not specified');
   });

   it('should fail with invalid name', () => {
      class MockSurveyElement extends SurveyElement {
         static type = 'testType';
       }

      expect(() => new MockSurveyElement({ /*...*/ })).toThrowError('Name must not be empty');

      [
         undefined, null, false, 0, NaN, ''
      ].forEach(name => expect(() => new MockSurveyElement({ name })).toThrowError('Name must not be empty'));

      [
         {}, [], /./, () => {}, 123, new Date()
      ].forEach(name => expect(() => new MockSurveyElement({ name })).toThrowError('Name must be a string'));

      [
         '1', '_', '$', '.'
      ].forEach(name => expect(() => new MockSurveyElement({ name })).toThrowError(/^Invalid name/));
   });

   it('should create new element', () => {
      class MockSurveyElement extends SurveyElement {
         static type = 'testType';
      }

      [
         { name: 'foo' },
         { name: 'a0' },
      ].forEach(model => {
         expect(() => new MockSurveyElement(model)).not.toThrowError();
      });
      
   });

   it('should return get/set name and statement', () => {
      class MockSurveyElement extends SurveyElement {
         static type = 'testType';
      }
      const name = 'testElement123';
      const statement = 'Test statement 123';

      const element = new MockSurveyElement({ name, statement });

      expect(element.getType()).toEqual('testType');
      expect(element.getName()).toEqual(name);
      expect(element.getStatement()).toEqual(statement);

      const newName = 'newElement_123';
      const newStatement = 'New Statement !!!';

      element.setName(newName);
      element.setStatement(newStatement);

      expect(element.getName()).toEqual(newName);
      expect(element.getStatement()).toEqual(newStatement);
   });

   it('should test conditionals', () => {
      class MockSurveyElement extends SurveyElement {
         static type = 'testType';
      }
      const name = 'testConditional';
      const enabledCond = { bar: { $gt: 10 } };
      const visibleCond = { foo: 'hello world' };

      const element = new MockSurveyElement({ name, enabledCond, visibleCond });

      expect(element.isVisible({ foo: 'something' })).toBeFalsy();
      expect(element.isVisible({ foo: 'hello world' })).toBeTruthy();

      expect(element.isEnabled({ bar: 0 })).toBeFalsy();
      expect(element.isEnabled({ bar: 20 })).toBeTruthy();

      expect(element.getEnabledCond()).toEqual(enabledCond);
      expect(element.getVisibleCond()).toEqual(visibleCond);

      expect(element.toJSON()).toEqual({
         type: 'testType',
         name,
         enabledCond,
         visibleCond
      });

      element.setVisibleCond();  // undefined / null...
      element.setEnabledCond(null);

      expect(element.isVisible({ foo: 'something' })).toBeTruthy();
      expect(element.isVisible({ foo: 'hello world' })).toBeTruthy();

      expect(element.isEnabled({ bar: 0 })).toBeTruthy();
      expect(element.isEnabled({ bar: 20 })).toBeTruthy();

      expect(element.getEnabledCond()).toBeFalsy();
      expect(element.getVisibleCond()).toBeFalsy();

      expect(element.toJSON()).toEqual({
         type: 'testType',
         name
      });

   });

   it('should set/get the required state', () => {
      class MockSurveyElement extends SurveyElement {
         static type = 'testType';
      }

      const name = 'testConditional';
      const required = true;
      const element = new MockSurveyElement({ name, required });

      expect(element.isRequired()).toBeTruthy();
      expect(element.toJSON()).toEqual({
         type: 'testType',
         name,
         required
      });

      element.setRequired(false);
      expect(element.isRequired()).toBeFalsy();
   });

   it('should set/get the validation message', () => {
      class MockSurveyElement extends SurveyElement {
         static type = 'testType';
      }

      const name = 'testConditional';
      const language = SurveyLocales.getDefaultLanguage();
      const validation$required = 'Required!';
      const element = new MockSurveyElement({ name, strings: { [language]: { validation$required } } });

      expect(element.getValidationMessage('required')).toEqual(validation$required);
      expect(element.toJSON()).toEqual({
         type: 'testType',
         name,
         strings: {
            [language]: {
               validation$required
            }
         }
      });

      element.setRequired(false);
      expect(element.isRequired()).toBeFalsy();


   });

   it('should get validation specs', () => {
      class MockSurveyElement extends SurveyElement {
         static type = 'testType';
      }
      const name = 'testElement';
      const element = new MockSurveyElement({ name });

      expect( getValidationSpecs(element) ).toEqual({
         type: SchemaType.Any,
         required: false
      });
   });

   it('should test exporting to JSON', () => {
      class MockSurveyElement extends SurveyElement {
         static type = 'testType';

         toJSON() {
            const json = super.toJSON();

            json.tested = true;

            return json;
         }
      }
      const name = 'testConditional';

      const element = new MockSurveyElement({ name });

      expect(element.toJSON()).toEqual({
         type: 'testType',
         name,
         tested: true
      });
   });


   describe('Testing type registration', () => {

      it('should register type', () => {
         const testType = 'testType';
         
         class MockSurveyElement extends SurveyElement {
            static type = testType;
         }

         expect(SurveyElement.getType(testType)).toBeUndefined();
         SurveyElement.registerType(MockSurveyElement);
         expect(SurveyElement.getType(testType)).toBe(MockSurveyElement);
         SurveyElement.unregisterType(testType);
         expect(SurveyElement.getType(testType)).toBeUndefined();
      });

      it('should fail registering type', () => {

         class MissingTypeMockSurveyElement extends SurveyElement {}

         expect(() => SurveyElement.registerType()).toThrowError('Type not specified')   // nothing
         expect(() => SurveyElement.registerType({})).toThrowError('Invalid element type');
         expect(() => SurveyElement.registerType(MissingTypeMockSurveyElement)).toThrowError('Type must define a valid static property type');

         [
            undefined, null, 0, 123, '', () => {}, new Date()
         ].forEach(invalidType => {
            class InvalidMockSurveyElement extends SurveyElement {
               static type = invalidType;
            }
            
            expect(() => SurveyElement.registerType(InvalidMockSurveyElement)).toThrowError('Type must define a valid static property type');
         });
         
      });

   });


   describe('Testing validation messages', () => {

      it('should get/set validation message', () => {
         class MockSurveyElement extends SurveyElement { 
            static type = 'testType';
         }

         const element = new MockSurveyElement({ name: 'test' });
         const testKey = 'testKey';
         const testMessage = 'Test Message';
         const testMessageZU = 'Test ZULU Message';
         
         expect( element.getValidationMessage(testKey) ).toBe(undefined);
         element.setValidationMessage(testKey, testMessage);
         expect( element.getValidationMessage(testKey) ).toBe(testMessage);

         element.setValidationMessage(testKey, testMessageZU, 'zu');
         expect( element.getValidationMessage(testKey, 'zu') ).toBe(testMessageZU);
         expect( element.getValidationMessage(testKey) ).toBe(testMessage);
      });

      it('should fail if key is not a string', () => {
         class MockSurveyElement extends SurveyElement { 
            static type = 'testType';
         }

         const element = new MockSurveyElement({ name: 'test' });

         [
            undefined, null, -1, 0, 1, NaN, Infinity,
            ()=>{}, [], {}, /./, new Date()
         ].forEach(key => {
            expect(() => element.getValidationMessage(key)).toThrow();
            expect(() => element.setValidationMessage(key)).toThrow();
         })
      });

   });

});