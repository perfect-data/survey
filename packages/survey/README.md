# Survey model

## Install

```
npm i @perfect-data/survey
```


## Usage

```js
import SurveyModel from '@perfect-data/survey';
import SurveyElement from '@perfect-data/survey/element';


class FooSurveyElement extends SurveyElement {
   static type = 'foo';

   getValidator(survey) {
      return {
         type: String,
         required: true
      };
   }
}

SurveyElement.registerType(FooSurveyElement);


const survey = new SurveyModel({
   title: 'Simple Survey',
   pages: [
      {
         elements: [
            { type: 'foo', name: 'name', statement: 'Enter your name?' },
            { type: 'foo', name: 'age', statement: 'What is your age, {{name}}?', visibleCond: { name: { $exists: true } } },
         ]
      }
   ]
});


const schema = survey.createSchema();
const validationContext = schema.createContext();
const response = schema.createModel();

response['name'] = 'John';

if (schema.validate(response)) {
   console.log("Valid!");
}
```


## License

MIT