import SurveyElement from './element';


/**
 * Define a survey page
 * 
 * Survey Page Model:
 * 
 *    - randomElements: Boolean
 *    - elements: Array<SurveyElement>
 * 
 */
class SurveyPage {

   /**
    * Create a new Page
    * @param {Object} model 
    */
   constructor(model) {
      this._randomElements = model && model.randomElements || false;
      this._elements = (model && model.elements || []).map(element => {
         if (!(element instanceof SurveyElement)) {
            const SurveyElementType = SurveyElement.getType(element.type);

            return new SurveyElementType(element);
         } else {
            return element;
         }
      });
   }


   /**
    * Return if the elements on this page should be displayed randomly
    * @return {Boolean}
    */
   hasRandomElements() {
      return this._randomElements;
   }

   /**
    * Set whether the elements on this page should be displayed randomly
    * @param {Boolean} randomElements 
    */
   setRandomElements(randomElements = true) {
      this._randomElements = !!randomElements;
   }

   
   /**
    * Add a new element to the page, at the given index if specified
    * @param {SurveyElement|Object} element 
    * @param {Number} index?
    */
   addElement(element, index) {
      if (!(element instanceof SurveyElement)) {
         const SurveyElementType = SurveyElement.getType(element.type);

         element = new SurveyElementType(element);
      }

      if (!isNaN(index)) {
         index = Math.max(Math.min(index, this._elements.length), 0);  

         this._elements.splice(index, 0, element);
      } else {
         this._elements.push(element);
      }
   }

   /**
    * Return the element at the specified index
    * @param {Number} index 
    * @return {SurveyElement}
    */
   getElement(index) {
      return this._elements[index];
   }

   /**
    * Return the elements on this page
    * @return {Array}
    */
   getElements() {
      return this._elements.slice();
   }

   /**
    * Return the number of elements on this page
    * @return {Number}
    */
   getElementCount() {
      return this._elements.length;
   }

   /**
    * Remove the given element at the specified index and return it
    * @param {Number} index 
    * @return {SurveyElement}
    */
   removeElement(index) {
      return this._elements.splice(index, 1).pop();
   }


   /**
    * Return a JSON object of this page and it's elements
    * @return {Object}
    */
   toJSON() {
      return {
         randomElements: this._randomElements,
         elements: this._elements.map(element => element.toJSON())
      };
   }

}



export default SurveyPage;