import Schema from '@perfect-data/schema';
import { allowedValuesValidator, restrictedValuesValidator, matchValidator } from '@perfect-data/schema-standard-validators';
import SurveyModel from './model';

Schema.use(allowedValuesValidator);
Schema.use(restrictedValuesValidator);
Schema.use(matchValidator);

export { default as SurveyElement } from './element';
export { default as SurveyPage } from './page';

export default SurveyModel;