import sift from 'sift';
import { LocalizedAbstract } from './locales';
import nameValidator from './util/name-validator';
import { validationSpec } from './decorators/element-validation';


const VALIDATION_KEY_PREFIX = 'validation$';
const typeRegistry = {};


/**
 * Base survey element. Element types need to extend this class and be registered.
 * 
 * Survey Element Model
 * 
 *    - name         : String
 *    - statement    : String
 *    - required     : Boolean
 *    - enabledCond  : Object
 *    - visibleCond  : Object
 * 
 */
class SurveyElement extends LocalizedAbstract {

   /**
    * Register a new type
    * @param {SurveyType} Type 
    */
   static registerType(Type) {
      if (!Type) {
         throw new TypeError('Type not specified');
      } else if (!(Type.prototype instanceof SurveyElement)) {
         throw new TypeError('Invalid element type');
      } else if (!isTypeValid(Type.type)) {
         throw new TypeError('Type must define a valid static property type');
      }

      typeRegistry[Type.type] = Type;
   }

   /**
    * Unregister type
    * @param {String} type 
    */
   static unregisterType(type) {
      delete typeRegistry[type];
   }

   /**
    * Return the SurveyElement type
    * @param {String} type 
    * @returns {SurveyElement}
    */
   static getType(type) {
      return typeRegistry[type];
   }


   @validationSpec('required')
   _required = false;

   _enabledCond = null;
   _visibleCond = null;


   /**
    * Create a new element
    * @param {Object} model 
    */
   constructor(model) {
      super(model);

      if (this.__proto__ === SurveyElement.prototype) {
         throw new TypeError('Cannot instanciate this class directly');
      } else if (!model) {
         throw new TypeError('Element model not specified');
      } else if (!isTypeValid(this.__proto__.constructor.type)) {
         throw new TypeError('Type not defined or invalid for ' + this.__proto__.constructor.name);
      }

      this._name = sanitizeName(model.name);
      
      if ('statement' in model) {
         this.setStatement(model.statement);
      }
      if ('required' in model) {
         this.setRequired(model.required);
      }
      if ('enabledCond' in model) {
         this.setEnabledCond(model.enabledCond);
      }
      if ('visibleCond' in model) {
         this.setVisibleCond(model.visibleCond);
      }
   }


   /**
    * Return the element type
    * @returns {String}
    */
   getType() {
      return this.__proto__.constructor.type;
   }

   /**
    * Return the name for this element
    * @returns {String}
    */
   getName() {
      return this._name;
   }

   /**
    * Set the name of this element
    * @param {String} name 
    */
   setName(name) {
      this._name = sanitizeName(name);
   }


   /**
    * Return the statement for this element
    * @param {String} lang     (optional)   the language to retrieve the statement
    * @returns {String}
    */
   getStatement(lang) {
      return this.getString('statement', lang);
   }

   /**
    * Set the statement for this element
    * @param {String} statement 
    * @param {String} lang     (optional)   the language to set the statement
    */
   setStatement(statement, lang) {
      this.setString('statement', statement, lang);
   }


   /**
    * Return the condition to have this element enabled
    * @returns {Object|null}
    */
   getEnabledCond() {
      return this._enabledCond;
   }

   /**
    * Set the condition to have this element enabled
    * @param {Object|null} enabledCond 
    */
   setEnabledCond(enabledCond) {
      this._enabledCond = enabledCond;
   }

   /**
    * Check if this element should be enabled. If no condition is defined, returns true.
    * @param {Object} data 
    * @returns {Boolean}
    */
   isEnabled(data) {
      if (this._enabledCond) {
         const query = sift(this._enabledCond);

         return query(data);
      } else {
         return true;
      }                
   }


   /**
    * Return if the element requires to have an answer
    * @return {Boolean}
    */
   isRequired() {
      return this._required || false;
   }

   /**
    * Sets if the element requires to have an answer.
    * @param {Boolean} required
    */
   setRequired(required) {
      this._required = !!required;
   }

   /**
    * Get the message to display when validating the value for a given property
    * @param {String} key                   the property key
    * @param {String} lang     (optional)   the language to retrieve the message
    */
   getValidationMessage(key, lang) {
      if (typeof key !== 'string') {
         throw new TypeError('key must be a string');
      }

      return this.getString(VALIDATION_KEY_PREFIX + key, lang);
   }

   /**
    * Set the message to display when validating the value for a specified property
    * @param {String} key                   the property key
    * @param {String} message 
    * @param {String} lang     (optional)   the language to set the message
    */
   setValidationMessage(key, message, lang) {
      if (typeof key !== 'string') {
         throw new TypeError('Key must be a string');
      }

      this.setString(VALIDATION_KEY_PREFIX + key, message, lang);
   }


   /**
    * Return the condition to have this element visible
    * @returns {Object|null}
    */
   getVisibleCond() {
      return this._visibleCond;
   }

   /**
    * Set the condition to have this element visible
    * @param {Object|null} visibleCond 
    */
   setVisibleCond(visibleCond) {
      this._visibleCond = visibleCond;
   }

   /**
    * Check if this element should be visible. If no condition is defined, returns true.
    * @param {Object} data 
    * @returns {Boolean}
    */
   isVisible(data) {
      if (this._visibleCond) {
         const query = sift(this._visibleCond);

         return query(data);
      } else {
         return true;
      }                
   }

   /**
    * Return a JSON object of this element
    * @returns {Object}
    */
   toJSON() {
      const data = {
         type: this.getType(),
         name: this._name
      };

      if (this._required) {
         data.required = true;
      }
      if (this._enabledCond) {
         data.enabledCond = this._enabledCond;
      }
      if (this._visibleCond) {
         data.visibleCond = this._visibleCond;
      }

      return {
         ...super.toJSON(),
         ...data
      };
   }

}


function isTypeValid(type) {
   return (typeof type === 'string') && type;
}

function sanitizeName(name) {
   if (!name) {
      throw new TypeError('Name must not be empty');
   } else if (!(typeof name === 'string')) {
      throw new TypeError('Name must be a string');
   } else if (!nameValidator(name)) {
      throw new TypeError('Invalid name : ' + name);
   }

   return name;
}


export default SurveyElement;