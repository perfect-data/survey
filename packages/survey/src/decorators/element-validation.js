import { SchemaType } from '@perfect-data/schema';


const $validationKey = Symbol('validation');

/**
 * Declare a field for validation
 * 
 * Usage :
 * 
 *    @validationSpecs('required')
 *    isRequired() { ... }
 * 
 *    @validationSpecs('custom', field => value => field.getPasswordConfirmation() !== value && 'mismatch')
 *    getPassword() { ... }
 *    getPasswordConfirmation() { .... }
 * 
 * @param {String} name 
 * @param {Function} initializer 
 * @param {Object} options 
 */
const validationSpec = (name, initializer, options) => (target, key) => {
   if (!target[$validationKey]) {
      Object.defineProperty(target, $validationKey, {
         value: {},
         writable: false,
         enumerable: false,
         configurable: false
      });
   }

   target[$validationKey][key] = {
      name,
      initializer,
      options,
      // NOTE : regardless where the spec is defined, we want to work on a specific object
      getValue: newTarget => typeof newTarget[key] === 'function' ? newTarget[key]() : newTarget[key]
   };
}

/**
 * Return the validation specification for the given target. If no validationSpec has been declared on
 * the target, then an error will be thrown.
 * 
 * @param {SurveyElement} target 
 * @param {SurveyModel} survey
 * @return {Object}
 */
const getValidationSpecs = (target, survey) => {
   if (!target[$validationKey]) {
      throw new Error('No validation specs defined for this object');   
   }

   const specs = { type: SchemaType.Any };  // default

   for (let key in target[$validationKey]) {
      const spec = target[$validationKey][key];
      const value = spec.getValue(target);
      const specValue = typeof spec.initializer === 'function' ? spec.initializer(value, target, survey, spec.options) : value;

      if (specValue !== undefined) {
         specs[spec.name] = specValue;
      }
   }
   
   return specs;
}



export { validationSpec, getValidationSpecs };