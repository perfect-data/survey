import Locales from 'iso-639-1';


/**
 * Module-wide variable
 */
let defaultLanguage = 'en';


const SurveyLocales = {
   /**
    * Get the default language to use
    * @return {String}
    */
   getDefaultLanguage() {
      return defaultLanguage;
   },

   /**
    * Set the default language to use
    * @param {String} lang 
    */
   setDefaultLanguage(lang) {
      if (!Locales.validate(lang)) {
         throw new Error('Invalid language code : ' + lang);
      }

      defaultLanguage = lang;
   }
};


/**
 * An abstract class that implements localization of strings
 */
class LocalizedAbstract {

   constructor(model) {
      if (this.__proto__ === LocalizedAbstract.prototype) {
         throw new TypeError('Cannot instanciate this class directly');
      }

      if (model && model.strings && (typeof model.strings === 'object')) {
         this._strings = Object.keys(model.strings).reduce((strings, lang) => {
            if (!Locales.validate(lang)) {
               throw new TypeError('Invalid language : ' + lang);
            }

            strings[lang] = model.strings[lang];

            return strings;
         }, {});
      } else {
         this._strings = {};
      }
   }


   /**
    * Get a string associated with the given key and lang.
    * If lang is not specified, the default language is used.
    * @param {String} key 
    * @param {String} lang    (optional)
    * @returns {String|undefined}
    */
   getString(key, lang) {
      lang = lang || defaultLanguage;

      if ((lang in this._strings) && (key in this._strings[lang])) {
         return this._strings[lang][key];
      } else if ((defaultLanguage in this._strings) && (key in this._strings[defaultLanguage])) {
         return this._strings[defaultLanguage][key];
      } else {
         // pick first one lang...
         const fallbackLang = this.getLanguages().find(lang => key in this._strings[lang]);

         if (fallbackLang) {
            return this._strings[fallbackLang][key];
         }         
      }
   }


   /**
    * Set a string associated with the given key and lang.
    * If lang is not specified, the default language is used.
    * @param {String} key 
    * @param {String} str
    * @param {String} lang    (optional)
    */
   setString(key, str, lang) {
      if ((lang !== undefined) && !Locales.validate(lang)) {
         throw new Error('Invalid language code : ' + lang);
      }
      
      lang = lang || defaultLanguage;

      if (typeof key !== 'string') {
         throw new TypeError('Key must be a string');
      } else if (typeof str !== 'string') {
         throw new TypeError('Not a string for ' + lang + ':' + key);
      }

      if (!(lang in this._strings)) {
         this._strings[lang] = {};
      }

      this._strings[lang][key] = str;
   }


   /**
    * Return all languages where strings exist
    * @return {Array}
    */
   getLanguages() {
      return Object.keys(this._strings);
   }


   /**
    * Return all the strings as JSON
    */
   toJSON() {
      const stringsCount = Object.keys(this._strings).reduce((count, lang) => (
         count + Object.keys(this._strings[lang]).reduce(count => count + 1, 0)
      ), 0);

      if (stringsCount > 0) {
         return {
            strings: this._strings
         };
      } else {
         return {};
      }
   }

}



export default SurveyLocales;
export { LocalizedAbstract, Locales };