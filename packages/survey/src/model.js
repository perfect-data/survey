import Schema from '@perfect-data/schema';
import SurveyPage from './page';
import { LocalizedAbstract } from './locales';
import { getValidationSpecs } from './decorators/element-validation';


/**
 * Define a survey
 */
class SurveyModel extends LocalizedAbstract {

   /**
    * Create a new survey
    * @param {Object} model 
    * @param {Object} options    (optional)
    */
   constructor(model, options) {
      super(model);

      if (model && model.title) {
         this.setTitle(model.title);
      }
      this._pages = (model && model.pages || []).map(page => page instanceof SurveyPage ? page : new SurveyPage(page));

      this.options = options || {};
   }


   /**
    * Get the survey title
    * @param {String} lang     (optional)   the language to retrieve the title
    * @return {String}
    */
   getTitle(lang) {
      return this.getString('title', lang);
   }

   /**
    * Set the title of the survey
    * @param {String} title
    * @param {String} lang     (optional)   the language to set the title
    */
   setTitle(title, lang) {
      this.setString('title', title, lang);
   }


   /**
    * Add a new page to the survey, at the given index if specified
    * @param {SurveyPage|Object} page
    * @param {Number} index?
    */
   addPage(page, index) {
      if (!(page instanceof SurveyPage)) {
         page = new SurveyPage(page);
      }

      if (!isNaN(index)) {
         index = Math.max(Math.min(index, this._pages.length), 0);  

         this._pages.splice(index, 0, page);
      } else {
         this._pages.push(page);
      }
   }

   /**
    * Return the page at the specified index
    * @param {Number} index 
    * @return {SurveyPage}
    */
   getPage(index) {
      return this._pages[index];
   }

   /**
    * Return the pages from this survey
    * @return {Array}
    */
   getPages() {
      return this._pages.slice();
   }

   /**
    * Return the number of pages from this survey
    * @return {Number}
    */
   getPageCount() {
      return this._pages.length;
   }

   /**
    * Remove the given page at the specified index and return it
    * @param {Number} index 
    * @return {SurveyPage}
    */
   removePage(index) {
      return this._pages.splice(index, 1).pop();
   }
   

   /**
    * Returns true if the survey has this element
    * @param {String} name 
    * @returns {Boolean}
    */
   hasElement(name) {
      return this.getPages().some(page => page.getElements().some(element => element.getName() === name));
   }


   /**
    * Create the validation schema for this survey
    * @param {Object} options        (optional)  options passed to PerfectSchema
    * @return {PerfectSchema}
    */
   createSchema(options) {
      const survey = this;
      const specs = this.getPages().reduce((specs, page) => page.getElements().reduce((specs, element) => {
         const name = element.getName();

         try {
            specs[name] = getValidationSpecs(element, survey);
         } catch (e) {
            /* ignore error */
         }

         return specs;
      }, specs), {});

      return new Schema(specs, options);
   }



   /**
    * Return a JSON object of this survey
    * @return {Object}
    */
   toJSON() {
      return { 
         ...super.toJSON(),
         pages: this._pages.map(page => page.toJSON())
      };
   }

}


export default SurveyModel;