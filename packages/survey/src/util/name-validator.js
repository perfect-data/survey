const pattern = /^[a-z][a-z0-9_]*$/i;
export default name => (typeof name === 'string') && pattern.test(name);