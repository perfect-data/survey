const path = require('path');
const fs = require("fs-extra"); 
const glob = require('glob');


const pkg = fs.readJsonSync('./package.json');

pkg.workspaces.forEach(workspace => {
   glob.sync(workspace).map(project => {
      const projPkg = fs.readJsonSync(path.join(project, 'package.json'));

      return {
         path: path.normalize(project),
         name: projPkg.name,
         dependencies: projPkg.dependencies ? Object.keys(projPkg.dependencies).filter(module => module.startsWith(pkg.namespace)) : []
      };
   }).forEach((project, _index, projects) => {

      project.dependencies.forEach(dependency => {
         const relatedProject = projects.find(project => project.name === dependency)

         if (relatedProject) {
            const srcPath = path.join(relatedProject.path, 'build');
            const dstPath = path.join(project.path, 'node_modules', relatedProject.name);

            const isBuilt = fs.existsSync(srcPath);

            if (isBuilt) {
               const isDirectory = fs.existsSync(dstPath) && fs.statSync(dstPath).isDirectory();

               if (isDirectory) {
                  fs.removeSync(dstPath);
               }
               
               fs.ensureDirSync(path.dirname(dstPath));
               fs.ensureSymlinkSync(srcPath, dstPath);

               console.log("[O] Symlink for", relatedProject.name, ">>", dstPath);
            } else {
               console.log("[E] Module", relatedProject.name, "has no build");
            }
         }
      });

   });
});